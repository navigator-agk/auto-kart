import math
import serial

def clamp(val: int)-> int:
    if val > 100:
        return 100


class Arduino(serial.Serial):
    """ Used to send commands to the Arduino connected to the motor controller. """
    def _init_(self, port, timeout=1.0):
        super().__init__(port=port, baudrate=115200, timeout=timeout)

    def send_cmd(self, propulsive_effort: float, brake: bool)-> None:
        
        gear = 1
        reverse: bool = True if propulsive_effort < 0.0 else False
        brake: bool = brake
        effort = clamp(int(math.fabs(propulsive_effort) * 100.0))

        crc_str = str(int(reverse)) + str(int(brake)) \
                    + str(int(effort)) + str(int(gear))
        crc = int(crc_str) % 256

        cmd = bytearray([int(199), int(reverse), int(brake), int(effort), 
                         int(gear), int(crc), int(200)])

        self.write(cmd)
        return


class SmartMotor(serial.Serial):
    """ Used to send commands to the Arduino connected to the motor controller. """
    def _init_(self, port, timeout=1.0):
        super().__init__(port=port, baudrate=38400, timeout=timeout)

    def send_cmd(self, steering_angle: float)-> None:
        CPR = 4000.0        # encoder counts per revolution
        gear_ratio = 40.0   # ratio corresponding to gear box attached to the SmartMotor (output) 1:40 (input)
        total_ratio = CPR * gear_ratio / 360.0
        p = round(steering_angle * total_ratio)

        cmd_string = f'p={p:d}\r' # Encoder count that corresponds to the specified steering angle

        self.write(cmd_string.encode('utf-8'))
        return