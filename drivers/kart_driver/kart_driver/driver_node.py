import time

import rclpy
from rclpy.node import Node
from rclpy.qos import qos_profile_sensor_data

from interfaces.msg import AutoKartControl

from kart_driver.serial_drivers import Arduino, SmartMotor


class KartDriver(Node):

    def __init__(self):
        super().__init__('kart_driver')

        # ==== Parameters ====
        sm_port   = self.declare_parameter('sm_serial_port', '/dev/ttyUSB0').get_parameter_value().string_value
        ard_port   = self.declare_parameter('ard_serial_port', '/dev/ttyACM0').get_parameter_value().string_value
        self.steering_limit = self.declare_parameter('steering_angle_limit', 25.0).get_parameter_value().double_value

        # ===== Subscriptions =====
        self.control_sub = self.create_subscription(AutoKartControl, 'control_cmd', 
                                                    self.control_callback, qos_profile=qos_profile_sensor_data)

        # ===== Serial Connections =====
        self.sm_serial = SmartMotor(port=sm_port)
        self.arduino = Arduino(port=ard_port)

        time.sleep(1.0)

        # Send command for SmartMotor script to start.
        self.sm_serial.write(f'RUN\r'.encode('utf-8'))

        self.get_logger().info(f'Kart driver is ready.')

    def control_callback(self, msg: AutoKartControl)-> None:

        # ===== Steering Conversion =====
        if(-self.steering_limit <= msg.steering_angle <= self.steering_limit):
            self.sm_serial.send_cmd(msg.steering_angle)
        
        # ===== Throttle Conversion =====
        if(-1.0 <= msg.propulsive_effort <= 1.0):
            self.arduino.send_cmd(msg.propulsive_effort, msg.brake)
        
        return


def main(args=None):
    rclpy.init(args=args)

    node = KartDriver()

    try:
        rclpy.spin(node)
        
    except:
        # Stop the motor and close serial before shutdown.
        node.sm_serial.write(f'END\r'.encode('utf-8'))
        node.sm_serial.close()

    finally:
        node.destroy_node()
        rclpy.shutdown()


if __name__ == '__main__':
    main()