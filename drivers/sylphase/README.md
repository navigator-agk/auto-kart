# Information
Must be run with the Sylphase software.

# Notes
1. Forest has updated the software to work with 20.04
2. Python code has not been tested with very high update rates. >200Hz
3. ROS2 python3 version ran about half the average rate as ros1 version using the simulated kf2 demo.The software was run on different computers so it might be tied to cpu performance of the machine.


