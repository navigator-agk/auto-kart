from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='sylphase',
            executable='driver',
            parameters = [
                {'port': 1234},
                {'child_frame_id': 'base_link'},
                {'decimation': 1}
            ]
        )
    ])