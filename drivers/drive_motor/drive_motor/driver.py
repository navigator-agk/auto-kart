import serial

import rclpy
from rclpy.node import Node

from geometry_msgs.msg import Twist


class Arduino(serial.Serial):
    """ Used to send commands to the Arduino connected to the motor controller. """
    def _init_(self, port):
        super().__init__(port=port, baudrate=115200)
        self.motion_state = {'reverse': False, 'brake': False,
                                'speed': 0.0, 'gear': 1}
    
    def calc_crc(self):
        """ Calculate the crc remainder for the current motion state"""
        crc_str = str(int(self.motion_state['reverse'])) \
                    + str(int(self.motion_state['brake'])) \
                    + str(int(self.motion_state['speed'])) \
                    + str(int(self.motion_state['gear']))
        return int(crc_str) % 256

    def send_cmd(self):
        
        crc = self.calc_crc()

        cmd = bytearray([int(199), int(self.motion_state['reverse']),
                         int(self.motion_state['brake']), int(self.motion_state['speed']), 
                         int(self.motion_state['gear']), int(crc), int(200)])

        self.write(cmd)


class DriveMotorDriver(Node):
    def __init__(self):
        super().__init__('drive_motor_driver')

        # Get serial port parameters
        port = self.declare_parameter('port', '/dev/ttyACM0').get_parameter_value().string_value
        self.max_speed = self.declare_parameter('max_speed', 15.0).get_parameter_value().double_value

        self.arduino = Arduino(port=port)

        self.sub = self.create_subscription(Twist, 'vehicle_twist', self.twist_callback, 1)

    def twist_callback(self, msg):
        """ Convert Twist to appropriate serial commands"""
        self.arduino.motion_state['reverse'] = True if msg.linear.x < 0 else False
        self.arduino.motion_state['speed'] = abs(msg.linear.x / self.max_speed)
        self.arduino.motion_state['brake'] = True if speed == 0 else 0

        self.arduino.send_cmd()

    def write_cllbk(self):
        self.serial.serial_send(
            self.reverse, self.brake, self.speed, self.gear)

        self.serial.serial_arduino.reset_input_buffer()


def main(args=None):
    rclpy.init(args=args)

    node = DriveMotorDriver()
    try:
        rclpy.spin(node)

    except KeyboardInterrupt:
        node.serial.serial_arduino.close()

    finally:
        node.destroy_node()
        rclpy.shutdown()


if __name__ == '__main__':
    main()
