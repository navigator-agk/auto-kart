import time
import serial

import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Joy

"""
    Available data from Xbox 360 Controller using Joy
    --------------------------------------------------
    Axes:
        axes[0]: Left Thumbstick-Horizontal,Range: [1,-1], 0 Centered, 1 Left
        axes[1]: Left Thumbstick-Vertical,	Range: [1,-1], 0 Centered, 1 Up
        axes[2]: Left Trigger,				Range: [1,-1], 1 when unpressed
        axes[3]: Right Thumbstick-Horizontal,Range: [1,-1], 0 Centered, 1 Left
        axes[4]: Right Thumbstick-Vertical, Range: [1,-1], 0 Centered, 1 Up
        axes[5]: Right Trigger,				Range: [1,-1], 1 when unpressed
        axes[6]: Horizontal D-Pad			Vales: [1], [0], [-1], 1 pressed left, -1 pressed right, 0 not pressed
        axes[7]: Verical D-Pad				Vales: [1], [0], [-1], 1 pressed up, -1 pressed down, 0 not pressed
    Buttons:
        buttons[0]: "A",					Values: [0], [1], 1 when pressed
        buttons[1]: "B",					Values: [0], [1], 1 when pressed
        buttons[2]: "X",					Values: [0], [1], 1 when pressed
        buttons[3]: "Y",					Values: [0], [1], 1 when pressed
        buttons[4]: Left Bumper,			Values: [0], [1], 1 when pressed
        buttons[5]: Right Bumper,			Values: [0], [1], 1 when pressed
        buttons[6]: "Back" Button,			Values: [0], [1], 1 when pressed
        buttons[7]: "Start" Button,			Values: [0], [1], 1 when pressed
        buttons[8]: Xbox Button,			Values: [0], [1], 1 when pressed
        buttons[9]: Left Thumbstick Press,  Values: [0], [1], 1 when pressed
        buttons[10]: Right Thumbstick Press,Values: [0], [1], 1 when pressed
        Notes:
            Button presses exhibit a bouncing behavior, multiple message will be sent for a single button press.
            Additional buttons are also available past Button[5]
"""
MAX_SPEED = 15  # meters/sec


class serial_cmds():
    def __init__(self, port, baud_rate, start_byte, end_byte):
        self.serial_arduino = serial.Serial(port, baud_rate)
        self.start_byte = start_byte
        self.end_byte = end_byte

    def serial_send(self, reverse, brake, speed, gear):
        crc_str = str(int(reverse))+str(int(brake)) + \
            str(int(speed))+str(int(gear))
        crc = int(crc_str) % 256
        cmd = bytearray([int(self.start_byte), int(reverse),
                         int(brake), int(speed), int(gear), int(crc), int(self.end_byte)])

        self.serial_arduino.write(cmd)


class EDriver(Node):
    def __init__(self):
        super().__init__('edriver')

        # Get serial port parameters
        port = self.declare_parameter('port', '/dev/ttyACM0').get_parameter_value().string_value
        start_byte = self.declare_parameter('start_byte', 199).get_parameter_value().integer_value
        end_byte = self.declare_parameter('end_byte', 200).get_parameter_value().integer_value
        baudrate = self.declare_parameter('baudrate', 115200).get_parameter_value().integer_value

        self.gear = 1
        self.count = 0
        self.brake = 0
        self.reverse = 0
        self.speed = 0

        self.serial = serial_cmds(port, start_byte, end_byte, baudrate)

        self.get_logger().info(
            f"Arduino port opened {port} at baud rate of {baudrate}")
        time.sleep(1)  # Letting Arduino sleep for 1 second to allow for reset.

        self.sub = self.create_subscription(Joy, 'joy', self.joy_callback, 1)

        self.timer = self.create_timer(0.05, self.write_cllbk)

    def joy_callback(self, msg):
        self.speed = abs((float(msg.axes[5])-1.0)/2)*100
        if msg.buttons[3] == 1:
            self.reverse = 1
            self.get_logger().warn(f"In Reverse!")
        else:
            self.reverse = 0

        if msg.buttons[1] == 1:
            self.brake = 1
            self.get_logger().warn(f"Brake is enabled.")
        else:
            self.get_logger().warn(f"Brake is disabled.")
            self.brake = 0

        if msg.buttons[5] == 1 and msg.buttons[4] == 0:
            if self.gear < 3:
                self.count += 1
                if self.count >= 2:
                    self.gear += 1
                    self.count = 0
            else:
                self.get_logger().warn(f"In Highest Gear {self.gear}")
            self.get_logger().info(f"In Gear {self.gear}")
        elif msg.buttons[5] == 0 and msg.buttons[4] == 1:
            if self.gear > 1:
                self.count += 1
                if self.count >= 2:
                    self.gear -= 1
                    self.count = 0
            else:
                self.get_logger().warn(f"In Lowest Gear {self.gear}")
            self.get_logger().info(f"In Gear {self.gear}")


    def write_cllbk(self):
        self.serial.serial_send(
            self.reverse, self.brake, self.speed, self.gear)

        self.serial.serial_arduino.reset_input_buffer()


def main(args=None):
    rclpy.init(args=args)

    node = EDriver()
    try:
        rclpy.spin(node)

    except KeyboardInterrupt:
        node.serial.serial_arduino.close()

    finally:
        node.destroy_node()
        rclpy.shutdown()


if __name__ == '__main__':
    main()
