import os
from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():
    ld = LaunchDescription()
    edriver_node = Node(
        package='emotor_driver',
        executable='joy_driver',
        parameters=[{
            'port': '/dev/ttyACM0',
            'start_byte': 199,
            'end_byte': 200,
            'baudrate': 115200,
        }],
    )
    joy_node = Node(
        package='joy',
        executable='joy_node',
    )

    ld.add_action(edriver_node)
    ld.add_action(joy_node)
    return ld
