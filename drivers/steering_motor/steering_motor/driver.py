import serial

import rclpy
from rclpy.node import Node

from interfaces.msg import SteeringStamped


class SteeringDriver(Node):

    def __init__(self):
        super().__init__('steering_driver')

        # ==== Parameters ====
        serial_port   = self.declare_parameter('serial_port', '/dev/ttyUSB0').get_parameter_value().string_value
        baudrate      = self.declare_parameter('baudrate', 38400).get_parameter_value().integer_value
        self.steering_limit = self.declare_parameter('steering_angle_limit', 25.0).get_parameter_value().double_value

        # ==== Subscriptions ====
        self.subscription = self.create_subscription(SteeringStamped, 'steer_cmd', self.steer_cb, 10)

        # Serial connection to SmartMotor
        self.sm_serial = serial.Serial(serial_port, baudrate, timeout=2.0)

        # Send command for SmartMotor script to start.
        self.sm_serial.write(f'RUN\r'.encode('utf-8'))

        self.get_logger().info(f'Steering driver is ready.')

    def steer_cb(self, msg: SteeringStamped):
        
        if(msg.steering_wheel_angle <= self.steering_limit and msg.steering_wheel_angle >= -self.steering_limit):
            CPR = 4000.0        # encoder counts per revolution
            gear_ratio = 40.0   # ratio corresponding to gear box attached to the SmartMotor (output) 1:40 (input)
            total_ratio = CPR * gear_ratio / 360.0
            p = round(msg.steering_wheel_angle * total_ratio)

            cmd_string = f'p={p:d}\r' # Encoder count that corresponds to the specified steering angle

            self.sm_serial.write(cmd_string.encode('utf-8'))


def main(args=None):
    rclpy.init(args=args)

    steering_driver = SteeringDriver()

    try:
        rclpy.spin(steering_driver)
        
    except:
        # Stop the motor and close serial before shutdown.
        steering_driver.sm_serial.write(f'END\r'.encode('utf-8'))
        steering_driver.sm_serial.close()

    finally:
        steering_driver.destroy_node()
        rclpy.shutdown()


if __name__ == '__main__':
    main()